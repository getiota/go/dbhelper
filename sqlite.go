// +build sqlite

package dbhelper

// We use this file during development to avoid the pain associated with postgres

import (
	"os"

	"gorm.io/driver/sqlite"
	"gorm.io/gorm"

	sqlite3 "github.com/mattn/go-sqlite3"
)

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

// Open opens the database
func Open() (db *gorm.DB, err error) {
	path, ok := os.LookupEnv("DB_PATH")
	if !ok {
		path = "db.sqlite3"
	}
	db, err = gorm.Open(sqlite.Open(path), &gorm.Config{})
	return
}

func IsDuplicateError(err error) bool {
	sqlerr, ok := err.(sqlite3.Error)
	if !ok {
		return false
	}
	return sqlerr.Code == 19
}
